﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FirstTryInAngular.Models;

namespace FirstTryInAngular.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public List<Product> ProductList = new List<Product>()
        {
            new Product { Name = "Cheesee", Manufacturer = "Minsk", Code = 123456},
            new Product {Name = "Vine", Manufacturer = "Spain", Code = 654321},
            new Product { Name = "Lemon", Manufacturer = "Brasil", Code = 098765 }
        };

        public IEnumerable<Product> Get()
        {
            return ProductList;
        }

        // GET api/values/id
        public Product Get(int id)
        {
            return ProductList[id];
        }

        // POST api/values
        [HttpPost]
        public void CreateProduct([FromBody]Product product)
        {
            ProductList.Add(product);
        }

        // PUT api/values/id
        [HttpPut]
        public void EditBook(int id, [FromBody]Product product)
        {
            ProductList[id] = product;
        }

        // DELETE api/values/id
        [HttpDelete]
        public void DeleteProduct(int id)
        {
            ProductList.RemoveAt(id);
        }
    }
}
