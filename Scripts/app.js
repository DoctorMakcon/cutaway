﻿(function () {
    var app = angular.module("cutaway", []);

    app.controller("KeySkillsController", function () {
        this.skills = keySkills;
    });

    var keySkills = [
        {
            name: "cs",
            description: "Strong knowledge of C#, .Net framework & Entity Framework. Including new standards C# 6.0 & .Net Framework",
            image: "cs.png"
        },
        {
            name: "js",
            description: "Good knowledge of JavaScript. I really love js because multi-paradigm style awesome",
            image: "js.png"
        },
        {
            name: "jquery",
            description: "JQuery makes easier selecting item from DOM. It's basic knowledge for each developer",
            image: "jquery.png"
        },
        {
            name: "aspnet",
            description: "Full understanding how works ASP.NET MVC framework, Web API & Razor engine",
            image: "aspnet.png"
        },
        {
            name: "angularjs",
            description: "Like to build single-page applications with AngularJS. AngularJS uses a declarative style of programming that much easier to create the ideal interface",
            image: "angularjs.png"
        },
        {
            name: "bootstrap",
            description: "Bootstrap allows to quickly create pretty interface. Every developer must know him!",
            image: "bootstrap.png"
        },
        {
            name: "css",
            description: "CSS is the 'language of the Web'. You must understand him like your native language",
            image: "css3.png"
        },
        {
            name: "html",
            description: "HTML is the markup language. How you can be Web-developer without understanding of HTML?",
            image: "html5.png"
        },
        {
            name: "cpp",
            description: "I started my programmer path with C & C++. I have some knowledges in networks (I'm studying in Cisco network academy). Have good understanding of OOP, ORM & SOLID principles",
            image: "cpp.png"
        }
    ];
})();